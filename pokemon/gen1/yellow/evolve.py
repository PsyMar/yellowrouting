'''Header for evolution methods and levels.
Level-evos are represented by an integer from 1 to 100, so non-level methods are numbered higher than 100.
look I had to document this somewhere.

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-14

Since:
    0.0.0a 2020-11-13
'''

##@brief Dictionary of evolutionary stone names to constants representing them as evolution methods.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-14
# @see TRADE_EVO
# @see asm_to_evolution_dict
stone_dict = {"FIRE_STONE":101, "THUNDER_STONE":102, "WATER_STONE":103,
               "LEAF_STONE":104, "MOON_STONE":105}

##@brief Evolves when traded.  Here for completeness.  Speedrun doesn't use it.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-13
# @see stone_dict
# @see asm_to_evolution_dict
TRADE_EVO = 106

##@brief Maximum value for an evolution condition.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-14
# @see TRADE_EVO
MAX_EVO = TRADE_EVO


def asm_to_evolution_dict(strlist):
    '''Converts a list of evolution strings to a single-mon evolution dictionary.

    The dictionary maps ints which represent either a level, item, or trade,
    to the pokemon that results from the evolution.  Levels are (theoretically)
    1-100, other values represent non-level evolution methods.

    Pokemon results are represented as strings, because these have to be in place
    before all pokemon are constructed.

    This isn't always a singleton, because eevee has 3 evolutions (in RBY).

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-14

    See also:
        stone_dict
        TRADE_EVO
    '''
    evodict = dict()
    for line in strlist:
        splitline = line[4:].split(', ')
        if splitline[0] == "EV_LEVEL":
            evodict[int(splitline[1].strip())] = splitline[-1]
        elif splitline[0] == "EV_TRADE":
            evodict[TRADE_EVO] = splitline[-1]
        elif splitline[0] == "EV_ITEM":
            evodict[stone_dict[ splitline[1] ]] = splitline[-1]
        elif splitline[0] != "0": # 0 is the end-of-data sentinel and is valid input
            raise ValueError("strlist in asm_to_evolution_dict has invalid format, value is \n"
                             + strlist.str())
    return evodict



__all__ = [stone_dict.__name__, TRADE_EVO.__name__, MAX_EVO.__name__,
           asm_to_evolution_dict.__name__]
