'''Contains a class with functions for calculating levels based off exp in gen 1.

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-14

Since:
    0.0.0a 2020-11-12
'''


def _level_to_exp_medium_fast(lv):
    '''Convert a pokemon's level to exp, using the medium-fast exp rate.

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-12
    '''
    if lv == 1:
        return 0
    else:
        return lv**3
    # end level_to_exp_medium_fast

def _level_to_exp_medium_slow(lv):
    '''Convert a pokemon's level to exp, using the medium-fast exp rate.

    Fun fact: If you don't special case level 1 you get a negative value.
    In gen 1/2 you can use this to overflow a not-normally-found level 1 pokemon
    all the way to level 100 with a single low-xp battle.

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-12
    '''
    if lv == 1:
        return 0
    else:
        return (lv**3)*6//5 - 15*(lv**2) + 100*lv - 140
    # end level_to_exp_medium_slow

def _level_to_exp_fast(lv):
    '''Convert a pokemon's level to exp, using the fast exp rate.

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-12
    '''
    if lv == 1:
        return 0 # special casing isn't necessary here, but for consistency
    else:
        return (lv**3)*4 // 5
    # end level_to_exp_fast

def _level_to_exp_slow(lv):
    '''Convert a pokemon's level to exp, using the slow exp rate.

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-12
    '''
    if lv == 1:
        return 0
    else:
        return (lv**3)*6 // 5
    # end level_to_exp_slow

def level_to_exp(exp_rate_function, lv):
    '''Convert a pokemon's level to exp, using the given exp rate.

    Since exp rates are expressed in terms of the level-to-exp function,
    this just calls that function.  May make code clearer though.

    Version:
        0.0.0a 2020-11-12

    Since:
        0.0.0a 2020-11-12
    '''
    return exp_rate_function(lv)

def exp_to_level(exp_rate_function, exp):
    '''Find the level of a pokemon, given its growth function and current exp.

    Version:
        0.0.0a 2020-11-12

    Since:
        0.0.0a 2020-11-12
    '''
    lv = 1
    while exp_rate_function(lv+1) < exp:
        lv += 1

    return lv

##@brief map that converts the single-digit codes the asm uses to exp rate functions
#@details 0 is medium_fast, 3 is medium_slow, 4 is fast, 5 is slow.
#This actually maps to the level_to_exp_(rate) functions, but those can be passed
#    to either level_to_exp or exp_to_level.
#
#@version 0.0.0a
#@date 2020-11-14
#@since 0.0.0a 2020-11-12
_exp_rates_dict = {0:_level_to_exp_medium_fast, 3:_level_to_exp_medium_slow,
             4:_level_to_exp_fast, 5:_level_to_exp_slow}

def exp_rates(intstr):
    '''Gets the exp rate function associated with a single-digit string in the code.

    It had to be named this or I had to change it in pokemon.py too.  Of course,
    I haven't actually committed that yet so I could change it all I wanted.

    ...Nevertheless, it persists in its naming.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-13
    '''
    return _exp_rates_dict[int(intstr)]

def validate_is_exp_rate(self, attribute, value):
    '''Attrs-compatible validator for exp rates.

    Args:
        self:        the object being constructed
        attribute:   the attribute being initialized
        value:       the value to validate is an exp rate

    Raises:
        TypeError if value is not a valid exp rate function as found in exp_rates_dict


    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-14
    '''
    if value not in _exp_rates_dict:
        raise TypeError('value passed to constructor for ' + self.str() +
                        " for attribute " + attribute.str() + " should be an "
                        "experience rate function, but instead is " + value.str())
    # end validate_is_exp_rate



__all__ = [exp_rates.__name__, level_to_exp.__name__, exp_to_level.__name__,
           validate_is_exp_rate.__name__]
