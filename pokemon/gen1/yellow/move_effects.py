'''Contains a dictionary that takes move effect strings and gives functions that represent the move.

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-14

Since:
    0.0.0a 2020-11-12
'''

from .options import asm_path

def _splash_effect(*args, **kwargs):
    '''Splash does nothing.  this also makes it a good placeholder function.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-12
    '''
    pass
    #end splash_effect

## @brief Path to move_effect_constants.asm.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-12
_move_effects_path = asm_path.joinpath("constants/move_effect_constants.asm")

##@brief Big dictionary of move effect functions.  currently just lets everything splash
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0, 2020-11-12
_move_effects_dict = dict()

with open(_move_effects_path) as effects_file:
    for line in effects_file:
        if "const " in line:
            _move_effects_dict[(line.split(" ")[1])] = _splash_effect

def move_effect(effect_name):
    '''Converts a move effect name, as found in move_effects.asm, to a Move.

    Args:
        effect_name:    the name of a move effect

    Returns:
        a function corresponding to effect_name

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-12
    '''
    return _move_effects_dict[effect_name]
# end move_effect


__all__ = [move_effect.__name__]
