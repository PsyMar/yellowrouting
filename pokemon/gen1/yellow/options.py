"""Compile-time options file for PsyMar's pokemon yellow routing program.

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-13

Since:
    0.0.0a 2020-11-13
"""

import pathlib.PurePath

## @brief string representing the path to the asm files for Pokemon Yellow.
# @version 0.0.0a
# @date 2020-11-13
# @since 0.0.0a 2020-11-13
_asm_folder = "./asm"
## @brief Path to the asm files for Pokemon Yellow.
# @version 0.0.0a
# @date 2020-11-13
# @since 0.0.0a 2020-11-13
asm_path = pathlib.PurePath(_asm_folder)

__all__ = [asm_path.__name__]
