#!/usr/bin/python3
"""Contains stat collation methods for pokemon yellow data and writes csv to
    a specified output file or standard output.

Example: 
    >>> python3 collate_stats.py
    Dex#,Name,HP,ATK,DEF,SPD,SPC,Type1,Type2,Catch Rate,Base Exp
    1,BULBASAUR,45,49,49,45,65,GRASS,POISON,45,64
    2,IVYSAUR,60,62,63,60,80,GRASS,POISON,45,141
    ...
"""

from .options import asm_path

#parse a single pokemon's data
def parse_one(mon_number, infile_path):
    """Parse a single pokemon's base stat, type, exp and catch rate data from sources
    and output a line of csv to out_file.

    Args:
        mon_number:     the current input line (dex number of the next mon)
        infile_path:    path to the file containing the next mon
    """
    with open(infile_path) as poke_data:
        #since in one case we must remove the first line, we can't just refer
        #to poke_data directly in the rest of the file, but must copy it
        lines = poke_data.read().splitlines()
        if lines[0][0:2] != "db": #there's a label at start of bulbasaur.asm
            lines = lines[1:] #remove the first line
        
        # each line starts with "db " then has the value we want, followed
        # by another space before a comment
        poke_name = lines[0].split(' ')[1].split('_')[1]
        # extracts the pokemon's name in all upper case, removing DEX_
        poke_hp = lines[1].split(' ')[1]
        poke_atk = lines[2].split(' ')[1]
        poke_def = lines[3].split(' ')[1]
        poke_spd = lines[4].split(' ')[1]
        poke_spc = lines[5].split(' ')[1]
        poke_type1 = lines[6].split(' ')[1]
        poke_type2 = lines[7].split(' ')[1]
        
        if(poke_type2 == poke_type1): # if types are the same, single-type mon
            poke_type2 = "" # so don't list a 2nd type
        catch_rate = lines[8].split(' ')[1]
        base_exp = lines[9].split(' ')[1]
        print(','.join( #join these values with a comma between each
            (str(mon_number), poke_name, poke_hp, poke_atk, poke_def, poke_spd,
            poke_spc, poke_type1, poke_type2, catch_rate, base_exp),
            ))
    #end of parse_one

# parse all the pokemon
def parse_all():
    """
        Parse data from all mons in base_stats.asm, one at a time, using parse_one.
        
        Also prints a header row before doing that, and prints all the data.
        
    """
    #print header row
    print("Dex#,Name,HP,ATK,DEF,SPD,SPC,Type1,Type2,Catch Rate,Base Exp")
    # get path to base_stats.asm, which lists filenames for each pokemon
    basestats_path = asm_path.joinpath("data/base_stats.asm")
    # number of the current pokemon; base_stats.asm has them in order
    mon_number = 1
    with open(basestats_path) as basestats_asm:
        for line in basestats_asm: #it automatically splits by lines
            # remove 'include "' and closing quote, then parse mon data
            parse_one(mon_number, asm_path.joinpath(line[9:-2]))
            # move to next mon
            mon_number += 1
    #end of parse_all
    
if __name__ == "__main__":
# if called directly
    parse_all()
# end program