'''Contains an enum of all the moves in gen 1 pokemon, along with relevant data on them.

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-14

Since:
    0.0.0a 2020-11-12
'''

from .options import asm_path
import attr
from .move_effects import move_effect
from .utility import stripstr_then_convert
from .utility import validate_range, lower_case_oneword
from .pokemontypes import pokemon_type

@attr.s(order=False, kw_only=True)
# no < or >; init with keywords
class _Move(object):
    """A move that a pokemon can use in battle.

    Members:
        NAME:       The name of the move in all uppercase.
        effect:     The function for the type of effect this move has. see move_effects.move_effect.
        move_power: The move's power, except the 5 moves with a nonstandard damage formula and the 3 healing-only moves.  these kinda do their own thing explained in comments where I set them.
        move_type:  The type of the move, e.g. pokemon_type("FIGHTING")
        accuracy:   The move's accuracy, on a 1-100 scale.
        base_maxPP: The maximum PP of the move when freshly learnt (no PP ups used).

    Notes:
        Negative move power means a percent.
        For healing moves, this is the percent of max HP healed.
        For fixed-damage moves, this is the percent of the pokemon's level done as damage.
        Fixed-damage moves that do the same damage no matter what (dragon rage, sonic boom) have positive power.
        Psywave is listed as doing fixed damage because its formula is not the normal damage formula
            (it does anywhere from 0 damage, or 1 if used by player, to 1.5x level).
            This is special-cased in move_effect("FIXED_DAMAGE_EFFECT") by checking for a power of -150.
            Also, the special-casing actually doesn't rule out 0 for a player's psywave, because nobody uses psywave
            and no other move has asymmetric effects like that.

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-12
    """
    NAME = attr.ib(converter = stripstr_then_convert(lower_case_oneword))
    effect = attr.ib(converter = stripstr_then_convert(move_effect))
    move_power = attr.ib(converter = stripstr_then_convert(int),
                         # explosion is max at 170, but is 250 in future games
                         validator = validate_range(-150,250))
    move_type = attr.ib(converter = stripstr_then_convert(pokemon_type))
    accuracy = attr.ib(converter = stripstr_then_convert(int),
                       validator = validate_range(0,100))
    base_maxPP = attr.ib(converter = stripstr_then_convert(int),
                         validator = validate_range(5,40))

## @fn _Move.__init__(*, NAME, effect, move_power, move_type, accuracy, base_maxPP)
# @arg NAME name of the move.  auto-converted to all-one-word-lower-case.
# @arg effect Function corresponding to the move's effect in battle
# @arg move_power Power of the move on offense, except for five moves that don't \
#      use the standard damage formulae and the three healing-only moves.
# @arg Type of the move (e.g. normal, flying, etc)
# @arg Chance to hit with no accuracy modifiers.  On a scale of 0 to 100.
# @arg The maximum PP a pokemon with this move has for it, if no PP ups or PP maxes are used.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-12

## @brief Path to moves.asm.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-12
_moves_path = asm_path.joinpath("data/moves.asm")

## @brief A string-keyed dictionary of _Move.
#
# Each move is classified by its name here, e.g. move_enum["TACKLE"] will get
# the _Move struct for tackle.
#
# @see moves._Move
# @version 0.0.0a
# @date 2020-11-12
# @since 0.0.0a 2020-11-12
_move_data = dict()
with open(_moves_path) as moves_file:
    for line in moves_file:
        if "\tmove " in line:
            moveline = line.split(',')
            moveline[0] = moveline[0].split(' ')[1]
            move = _Move(NAME       = moveline[0],
                        effect     = moveline[1],
                        move_power = moveline[2],
                        move_type  = moveline[3],
                        accuracy   = moveline[4],
                        base_maxPP = moveline[5])

            _move_data[move.NAME] = move

# struggle does half damage recoil while other recoil moves do 1/4
# fixed-damage moves do not all do the same damage.  store their damage in move_power
_move_data["sonicboom"].move_power = 20
_move_data["dragonrage"].move_power = 40
# for moves that do N% of a pokemon's level, use -N
_move_data["nightshade"].move_power = -100 # minus means level multiplier pct
_move_data["seismictoss"].move_power = -100 # ditto
# psywave is weird.  it does from 0 to 1.5*level.  just say here it does 150% of level and we can special case it in the effect function
# actually it does 1 damage min, not 0, when used by player.  but why are you using it
_move_data["psywave"].move_power = -150
# rest heals fully, other healing moves heal 50%.  rest also sleeps two turns but we can special case that.
# use healing moves' move_power to track a percent of HP healed, negative for consistency with the fixed-damage moves.
_move_data["rest"].move_power = -100
_move_data["recover"].move_power = -50
_move_data["softboiled"].move_power = -50

# aliases
_move_data["highjumpkick"] = _move_data["hijumpkick"] # changed in newer games
_move_data["visegrip"] = _move_data["vicegrip"] # also changed in newer games
_move_data["psychic"] = _move_data["psychicm"] # game data calls it PSYCHIC_M to avoid conflict with the PSYCHIC pokemon type
    # but absolutely no user is going to think of that


def move_from_name(name_of_move):
    '''Convert a string to a _Move.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-13

    See also:
        _move_data
        _Move
    '''
    return _move_data[name_of_move.strip().lower_case_oneword()]


__all__ = [move_from_name.__name__]
