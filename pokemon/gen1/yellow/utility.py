"""This file contains utility functions for PsyMar's pokemon routing program.

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-13

Since:
    0.0.0a 2020-11-12
"""

import string # for capwords

def validate_upper_no_spaces(self, attribute, value):
    '''Validates that a string is all uppercase and has no spaces.

    Non-cased characters are fine.
    For use with the attrs library.
    Does not return anything.

    Args:
        self:    An object being constructed.
        attribute:    the attribute of that object to which value is being passed.
        value:    The value to be validated.

    Raises:
        ValueError if the validation fails.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-12
    '''
    if not value.islower():
        raise ValueError("String that should be all uppercase has lowercase!"
                           "\nself, attribute, value:\n" +
                           (self, attribute, value).str())

    elif ' ' in value:
        raise ValueError("String has a space, should be an underscore!\n"
                           "self, attribute, value:\n" +
                           (self, attribute, value).str())
    # end def validate_upper_no_space


def stripstr_then_convert(base_converter):
    '''Returns a function which pre-processes a string before converting.

    Args:
        base_converter:    A one-argument conversion function that accepts strings.

    Returns:
        A lambda which converts its one argument to a string, strips whitespace,
        then does whatever base_converter does.

    Version:
        0.0.0a 2020-11-12

    Since:
        0.0.0a 2020-11-12
    '''
    # converts to string, then strips, then runs base_converter
    return lambda v:(None if v is None else base_converter(str(v).strip())) # hope this works
    #end def stripstr_then_convert

def title_case_oneword(name):
    '''Capitalizes each word in name and removes all underscores or spaces or hyphens.

    Underscores count as word separators.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-12
    '''
    name2 = name.replace('_', ' ').replace('-',' ')
    return string.capwords(name2) # standard python library
    #end def title_case_oneword

def lower_case_oneword(name):
    '''Removes underscores and spaces and hyphens and converts to all lower-case.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-12
    '''
    splitname = name.replace('_', ' ').replace('-',' ').split()
    return ''.join(splitname).lower()
    # end def lower_case_oneword

def validate_range(low, high): #inclusive
    '''Creates a validator for attrs that allows values only between low and high.

    Values are inclusive.  If the value is outside of the range, the resulting
    validator will throw ValueError, however this function itself does not throw.

    The returned function does not return a value when called.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-12
    '''
    def new_validator(self, attribute, value):
        if value < low:
            raise ValueError("Value too low: " + value.str() +
                             " for attribute " + attribute.str() +
                             " of " + self.str())
        elif value > high:
            raise ValueError("Value too high: " + value.str() +
                             " for attribute " + attribute.str() +
                             " of " + self.str())
        # end def new_validator
    return new_validator
    # end def validate_range

def validate_range_iterable(low,high): # calls validate_range on every member of an iterable
    '''Works like validate_range but on each item of iterables, not on scalars.

    If any value of an iterable is outside of the range low to high inclusive,
    the validator which this function returns will throw ValueError.

    The returned function does not return a value when called.

    Version:
        0.0.0a 2020-11-13

    Since:
        0.0.0a 2020-11-12
    '''
    def new_validator(self, attribute, iterable_value):
        for value in iterable_value:
            validate_range(low,high)(self, attribute, value)
            # this isn't perfect in terms of error messages but it'll do
    return new_validator
    # end def validate_range_iterable


# look do I really need a doc comment on __all__????
__all__ = [validate_upper_no_spaces.__name__, stripstr_then_convert.__name__,
           title_case_oneword.__name__, lower_case_oneword.__name__,
           validate_range.__name__, validate_range_iterable.__name__]
