"""This module contains the _pokemon_types enum which also has type-effectiveness data.
These effectiveness values are stored as _pokemon_types.att_type.effectiveness[def_type].

Copyright:
    Copyright 2020 psymar@fastmail.com.  MIT licensed.

Author:
    https://gitlab.com/psymar

Version:
    0.0.0a 2020-11-14

Since:
    0.0.0a 2020-11-13
"""

from .options import asm_path
import enum

## @brief Path to type_constants.asm
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-13
_type_constants_path = asm_path.joinpath("constants/type_constants.asm")

##@brief list of pokemon type names used to construct _pokemon_types enum
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-13
_type_names = ["NOTYPE2"] # placeholder for pokemon with no 2nd type
with open(_type_constants_path) as types_file:
    for line in types_file:
        if 'EQU' in line: # only lines with this string have a type in them
            _type_names.append(line.split(' ')[0])
            # everything before the first space
# now make the list into an enum
## @brief An enum of pokemon types with type relationship info.
# @details _pokemon_types[att_type].effectiveness[def_type] gives 0, 0.5, 1 or 2
# depending on the effectiveness of an att_type attack on a def_type mon.
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-13
_pokemon_types = enum.IntEnum('_pokemon_types', _type_names, start=0, module = __name__)
# starting at 0 is important as now it can index tuples
# thereby getting around python's lack of frozendict

##@brief path to type_effects.asm
# @version 0.0.0a
# @date 2020-11-14
# @since 0.0.0a 2020-11-13
type_chart_path = asm_path.joinpath("data/type_effects.asm")

# type effectiveness map.  1 means normal, 0 none, 0.5 not very, 2 super
for att_type in _pokemon_types:
    att_type.effectiveness = [1 for def_type in _pokemon_types]
    # initialize every matchup to 1, i.e. normally effective

with open(type_chart_path) as type_effect_data:
    for line in type_effect_data:
        if "db" in line and "," in line:
            splitline = line.split(" ")[-1].split(",")
            # everything after the last space, split by commas
            att_type = _pokemon_types[splitline[0]] # attack type
            def_type = _pokemon_types[splitline[1]] # defending type
            effect = int(splitline[2],10) / 10
            # type-effectiveness multiplier
            # the source file uses 0/5/10/20 instead of 0/0.5/1/2
            att_type.effectiveness[def_type] = effect

for att_type in _pokemon_types:
    att_type.effectiveness = tuple(att_type.effectiveness)
    # make them more-or-less immutable as no more changes should be made

def pokemon_type(name):
    '''Convert a string to a pokemon type value.

    Version:
        0.0.0a 2020-11-14

    Since:
        0.0.0a 2020-11-13
    '''
    return _pokemon_types[name]


__all__ = [pokemon_type.__name__]
