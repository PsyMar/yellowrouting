# PsyMar's Pokemon Yellow Routing Programs

For licensing and contact information, please see LICENSE.txt.

Utilities for routing pokemon yellow speedruns using data extracted from the
game's .asm files (which I found at https://github.com/pret/pokeyellow )

